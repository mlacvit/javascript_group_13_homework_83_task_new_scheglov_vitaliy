const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const MusSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    artist: {
        type: String,
    },
    year: String,
    image: String
});

const MusAlbums = mongoose.model('album', MusSchema);

module.exports = MusAlbums;